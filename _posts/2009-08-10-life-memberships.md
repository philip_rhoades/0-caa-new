---
layout: post
title:  "Life Memberships"
author: Philip Rhoades
date:   2009-08-10 12:00:00 +1000
categories: memberships
---

At a meeting of the CAA Executive via phone conference on 10 August, 2009 we
resolved to create a new category of membership called "Life Membership" (LM) where:

* LMs are exempt from paying further annual CAA dues (although they are still
encouraged to make donations to CAA)
* people are eligible for LM when the total of their contributions reaches $2,500
* people who reach eligibility for LM will have the LM conferred at the following
AGM
* when we send out the membership renewals we will include information about the
total payments to date and how much would be required to reach LM

We hope these changes will help keep members financial and provide an incentive
to reach LM. 


