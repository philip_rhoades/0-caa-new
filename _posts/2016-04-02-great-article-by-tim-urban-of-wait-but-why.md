---
layout: post
title:  "Great Article by Tim Urban of Wait But Why"
author: Philip Rhoades
date:   2016-04-02 12:00:00 +1000
categories: article
---

Probably the best and most comprehensive discussion to date on why Cryonics is a good idea:

[ Why Cryonics Makes Sense ]( http://waitbutwhy.com/2016/03/cryonics.html )

