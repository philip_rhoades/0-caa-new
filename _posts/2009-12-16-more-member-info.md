---
layout: post
title:  "More Member Info"
author: Philip Rhoades
date:   2009-12-16 12:00:00 +1000
categories: memberships
---

Two more sections added to the web site that can only be accessed by members.
The extra information includes summary payment details for the logged-in member
(including a balance required for Life Membership status) and CAA
organisational documentation that members need to have access to.

Every member who wants to be able access these extra sections will need to be
set up as a user of the form:

&nbsp;&nbsp;&nbsp;&nbsp;joe.bloggs

with appropriate password. 

UPDATE 2013.02.2 - these facilities have had little use and have been
discontinued in the updated web site.  If there is renewed enthusiam for this
sort of facility in the future, it may be restored.


