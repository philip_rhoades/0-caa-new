---
layout: post
title:  "CAA AGM"
author: Philip Rhoades
date:   2009-04-18 12:00:00 +1000
categories: meetings
---

We have just had the 2009 AGM and I have updated the web site - including a new
page for Non Members.  The CAA has incurred significant costs in the past by
supporting Cryonics cases that were not members.  We can't continue to do this
and have implemented a staged set of charges for these situations.


