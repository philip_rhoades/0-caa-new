---
layout: post
title:  "Would you want to be frozen and thawed out some years later?"
author: Philip Rhoades
date:   2024-05-23 12:00:00 +1000
categories: patient1
---

myLot: 

[ Would you want to be frozen and thawed out some years later? ]( https://www.mylot.com/post/3591973/would-you-want-to-be-frozen-and-thawed-out-some-years-later )

"Hitting our news a couple of days ago was the fact that Australia’s first known cryonics facility has just deep frozen its first client, a man in his 80s. The client died on May 12 in a Sydney hospital and the 10-hour process of preserving his body began immediately.

His body was brought down to a temperature of minus 200 degrees then placed in a pod and lowered into a vacuum storage vessel similar to a giant thermos.

This process is very controversial and some experts say it will be a long road before they’re able to revive the body and fix whatever killed the person in the first place. I wouldn’t want this to happen to me. When would you be revived, who would be around to care and what might the world be like when you returned? No thanks."

See full information at link above.


