---
layout: post
title:  "Australia's only cryonics facility welcomes first frozen 'patient'"
author: Philip Rhoades
date:   2024-04-21 12:00:00 +1000
categories: patient1
---

The Bottom Drawer: 

[ Australia's only cryonics facility welcomes first frozen 'patient' ]( https://thebottomdrawerbook.com.au/2024/05/21/australias-only-cryonics-facility-welcomes-first-frozen-patient/ )

"A man who died in a Sydney hospital on May 12 has become the first person to be frozen and stored in Australia’s only cryonics facility. If and when the man in his 80s will one day be able to be revived is anyone’s guess."

See full information at link above.


