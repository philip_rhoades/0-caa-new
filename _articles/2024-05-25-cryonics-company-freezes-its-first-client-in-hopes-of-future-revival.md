---
layout: post
title:  "Cryonics Company Freezes Its First Client In Hopes Of Future Revival"
author: Philip Rhoades
date:   2024-05-25 12:00:00 +1000
categories: patient1
---

India Times: 

[ Cryonics Company Freezes Its First Client In Hopes Of Future Revival ]( https://www.indiatimes.com/trending/wtf/cryonics-company-freezes-its-first-client-in-hopes-of-future-revival-635015.html )

"A cryonics company has frozen its first client in the hopes of one day bringing him back to life. Southern Cryonics' Philip Rhoades reported that the company had successfully cryogenically frozen a man from Sydney. The man died in his eighties earlier this month. "[It was] very stressful," Rhoades told ABC News."

See full information at link above.


