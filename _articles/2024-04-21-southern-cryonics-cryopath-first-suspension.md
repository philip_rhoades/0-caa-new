---
layout: post
title:  "Southern Cryonics/CryoPath First Suspension"
author: Philip Rhoades
date:   2024-04-21 12:00:00 +1000
categories: patient1
---

Southern Cryonics: 

[ Southern Cryonics/CryoPath First Suspension ]( https://southerncryonics.com/southern-cryonics-cryopath-first-suspension/ )

"May 21st, 2024 by pstsola

We are pleased to announce that Southern Cryonics (SC) and CryoPath successfully completed their first cryonics suspension with Patient 1 from May 12-17, 2024.

After learning of Patient 1’s deteriorating condition, we swiftly responded to his passing on May 12 in Sydney. Thanks to the quick action of our team, including Phil Rhoades, Australian Blood Management (ABM), and the cooperation of hospital staff, we promptly began the suspension process. Patient 1 was transferred from the hospital’s cooling room to A. O’Hare funeral home, packed in ice. We appreciate the funeral director’s immediate support."

See full information at link above.

