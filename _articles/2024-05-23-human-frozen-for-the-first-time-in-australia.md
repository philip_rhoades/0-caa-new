---
layout: post
title:  "Human frozen for the first time in Australia"
author: Philip Rhoades
date:   2024-05-23 12:00:00 +1000
categories: patient1
---

ABC listen: 

[ Human frozen for the first time in Australia ]( https://www.abc.net.au/listen/programs/melbourne-breakfast/human-frozen-for-the-first-time-in-australia-/103882408 )

"Is it realistic? Is it worth it? Some Australians seem to think so, as you can now have your body frozen with the hope that one day you'll be removed from the fridge and reanimated.

Southern Cryonics is the first facility of its kind in Australia, and its founder Peter Tsolakides told Sammy J about their first 'suspension'."

See full information at link above.


