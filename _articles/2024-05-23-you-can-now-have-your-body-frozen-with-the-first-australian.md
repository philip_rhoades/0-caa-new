---
layout: post
title:  "You can now have your body frozen with the first Australian now in 'suspension'"
author: Philip Rhoades
date:   2024-05-23 12:00:00 +1000
categories: patient1
---

ABC News - YouTube: 

[ You can now have your body frozen with the first Australian now in 'suspension' ]( https://www.youtube.com/watch?v=HmrWt56RXfc )

"Is it realistic? Is it worth it? Some Australians seem to think so, as you can now have your body frozen with the hope that one day you'll be removed from the fridge and reanimated.

Southern Cryonics is the first facility of its kind in Australia, and its founder Peter Tsolakides discusses their first 'suspension'."

See full information at link above.


