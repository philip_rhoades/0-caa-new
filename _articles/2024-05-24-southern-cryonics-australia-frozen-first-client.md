---
layout: post
title:  "Cyronics company freezes its first client in hope of bringing him back to life in future"
author: Philip Rhoades
date:   2024-05-24 12:00:00 +1000
categories: patient1
---

LAD Bible: 

[ Cyronics company freezes its first client in hope of bringing him back to life in future ]( https://www.ladbible.com/news/world-news/southern-cryonics-australia-frozen-first-client-144453-20240524 )

"A cryonics company has frozen its first ever client in the hopes that he can be brought back to life in the future.

Philip Rhoades, of Southern Cryonics, has announced that the company has successfully cryogenically frozen a man from Sydney.

The man passed away in his 80s earlier this month.

'[It was] very stressful,' Rhoades told ABC News."

See full information at link above.


