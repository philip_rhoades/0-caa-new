---
layout: post
title:  "Southern Hemisphere's first cryogenically frozen client at rest in regional New South Wales facility"
author: Philip Rhoades
date:   2024-05-23 12:00:00 +1000
categories: patient1
---

ABC News: 

[ Southern Hemisphere's first cryogenically frozen client at rest in regional New South Wales facility ]( https://www.abc.net.au/news/2024-05-23/sydney-man-first-cryogenically-frozen-holbrook-regional-nsw/103879454 )

"In short: A cryonics company has frozen its first client in Australia in the hope of bringing him back to life in the future. 

The client, a man in his 80s, died in Sydney before being frozen at minus 200 degrees Celsius at a Holbrook facility.

What's next? The cryonics facility is expecting higher demand as its membership base ages, although it's still unknown whether anyone preserved this way can ever be revived.
Philip Rhoades has spent 14 years waiting to give dead people a second chance at life.

Those years of preparation have finally been put to the test."

See full information at link above.


