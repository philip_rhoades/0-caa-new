---
layout: post
title:  "Sydney man first patient stored at Holbrook cryonics facility"
author: Philip Rhoades
date:   2024-05-23 12:00:00 +1000
categories: patient1
---

The Border Mail: 

[ Sydney man first patient stored at Holbrook cryonics facility ]( https://www.bordermail.com.au/story/8637167/sydney-man-first-patient-stored-at-holbrook-cryonics-facility/ )

"A human storage building at Holbrook has housed its first patient with the aim to bring them back to life in the next 200 years."

See full information at link above.


