---
layout: post
title:  "Boffin bought bags of ice in shop before cryogenically freezing dead man"
author: Philip Rhoades
date:   2024-05-24 12:00:00 +1000
categories: patient1
---

Daily Star:

[ Boffin bought bags of ice in shop before cryogenically freezing dead man ]( https://www.dailystar.co.uk/news/weird-news/boffin-bought-bags-ice-shop-32885527 )

"A Southern Cryonics client who died at a hospital in Sydney, Australia, was pumped with a 'type of anti freeze' before being lowered into a container and preserved.

A boffin who runs a 'Star Trek' cryogenics firm popped into a shop for bags of ice before freezing the body of a dead man.

Philip Rhoades who runs Southern Cryonics in Australia said he and his team were caught off guard when the family of the 80-year-old called up "out of the blue" to enquire about their services.

Philip, 72, told ABC that the man was not an existing candidate for freezing, but that his team had most of the equipment tested and ready to go by that stage."

See full information at link above.


