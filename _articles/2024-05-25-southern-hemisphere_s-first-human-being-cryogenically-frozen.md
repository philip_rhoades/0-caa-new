---
layout: post
title:  "Southern Hemisphere's First Human Being Cryogenically Frozen"
author: Philip Rhoades
date:   2024-05-25 12:00:00 +1000
categories: patient1
---

Pedestrian TV: 

[ Southern Hemisphere_s First Human Being Cryogenically Frozen ]( https://www.pedestrian.tv/news/cryogenically-frozen-australia/ )

"A facility in regional New South Wales has become the first in the Southern Hemisphere to cryogenically freeze a human person. I’ll be honest, this isn’t the sort of news story I thought I’d be writing this week but here we go.

At the Holbrook facility of Southern Cryonics, a Sydney man became the first person to be made into a human ice block. Holbrook is home to just under 2000 (living, unfrozen) people.

Australia has finally frozen its first bloke. What a week for the country.

The man, who was in his 80s when he passed away earlier in May, is now being referred to by the company formed in February of 2023, as “patient one”."

See full information at link above.


